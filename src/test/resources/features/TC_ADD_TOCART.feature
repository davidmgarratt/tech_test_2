@login
  Feature: ADD PRODUCT FROM FLIPKART AND VERIFY ITS ADDED TO BASKET
    Scenario: ADD PRODUCT AND VERIFY
      Given User launch Product using  "https://www.flipkart.com/kall-z5-green-16-gb/p/itmd861401a8b939?pid=MOBG7FGHHHHZFHZ9&lid=LSTMOBG7FGHHHHZFHZ9NKZRHI&marketplace=FLIPKART&q=mobile&store=tyy%2F4io&srno=s_1_1&otracker=search&otracker1=search&fm=organic&iid=en_YDYJv%2FZovC2dY3Qv3hfXR8XGCSrMTmmUHfGSu3i8%2FBp5z04nOPFUXfTZl1EMhvZVhJSQBUZbB%2BLtxCl4HRrs8w%3D%3D&ppt=hp&ppn=homepage&ssid=ebf35z6bhs0000001649842427392&qH=532c28d5412dd75b" in "chrome" browser
      When User Add Product To Basket
      Then Go to basket and verify the product is added
