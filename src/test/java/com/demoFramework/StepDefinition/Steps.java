package com.demoFramework.StepDefinition;


import com.demoFramework.pageObejcts.ProductPage;
import com.demoFramework.testCases.Setup;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

public class Steps extends Setup{
   ProductPage pp;
    WebDriverWait wait;

public  WebDriver driver;
    @Given("User launch Product using  {string} in {string} browser")
    public void user_launch_product_using_in_browser(String app_url, String browser) {
       try{
        driver= setup(driver,browser);
        driver.get(app_url);
        wait=setupWait(driver);}

       catch (Exception e){
           System.out.println("Unable to load the driver");
       }

    }

    @When("User Add Product To Basket")
    public void user_add_product_to_basket() {
       try{
           pp=new ProductPage(driver,wait);

           pp.clickAddToCartElement();
       }

       catch(Exception e) {

           System.out.println("Unable to Add to Cart");

       }

    }


    @Then("Go to basket and verify the product is added")
    public void go_to_basket_and_verify_the_product_is_added() {
        try{


            Assert.assertTrue(pp.geturl().toLowerCase().contains("viewcart"));

        tearDown(driver);
        }

        catch (Exception e){

            System.out.println("Product is not added successfully");

        }
    }





}



