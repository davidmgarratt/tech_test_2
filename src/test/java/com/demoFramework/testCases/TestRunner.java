package com.demoFramework.testCases;
import io.cucumber.junit.Cucumber;

import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeUnit;
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "C:\\Demo_REPL\\src\\test\\resources\\features"
        ,glue={"com.demoFramework.StepDefinition"},plugin = {"pretty","html:target/cucumber-html-report"}
)

public class TestRunner {



}