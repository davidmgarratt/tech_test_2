package com.demoFramework.pageObejcts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductPage {

    WebDriver driver;
    WebDriverWait wait;

    By AddToCart= By.xpath("//*[@id=\"container\"]/div/div[3]/div[1]/div[1]/div[2]/div/ul/li[1]/button");
    public ProductPage(WebDriver driver,WebDriverWait wait){
        this.driver=driver;
        this.wait=wait;

    }

    public void clickAddToCartElement(){
        wait.until(ExpectedConditions.elementToBeClickable(AddToCart));
        driver.findElement(AddToCart).click();}

        public  String geturl(){


          String Current=driver.getCurrentUrl();
          return Current;

        }

}

