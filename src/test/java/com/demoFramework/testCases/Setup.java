package com.demoFramework.testCases;

import io.cucumber.java.AfterAll;
import io.cucumber.java.BeforeAll;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class Setup {

    String driverPath;



    public WebDriver setup(WebDriver driver,String browser){

        if(browser.contains("chrome")) {
            driverPath="C:\\Demo_REPL\\Drivers\\chromedriver.exe";
            System.setProperty("webdriver.chrome.driver", driverPath);
            driver = new ChromeDriver();
        }
        else {
            driverPath="C:\\Demo_REPL\\Drivers\\geckodriver.exe";
            System.setProperty("webdriver.gecko.driver", driverPath);
            driver = new FirefoxDriver();
        }
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

    return driver;
    }


    public WebDriverWait setupWait(WebDriver driver){
        WebDriverWait wait =new WebDriverWait(driver, Duration.ofSeconds(5));

        return wait;
    }


   public void tearDown(WebDriver driver){
      driver.quit();
   }
}
